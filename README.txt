Mininet Project

Rumeet Goradia; rug5; 177008120
Nihar Prabhala; np642; 179005552

1) Briefly discuss how you implemented each functionality: setting up interfaces, setting up default routes, 
and setting up per-destination routes.

    We implemented the functionality of setting up interfaces using the 'ip addr add' command. The syntax of
    the command was '[hostname] ip addr add [destination ip] dev [corresponding port]'. In order to set up
    default routes, we used the 'ip route add default' command. The syntax was '[hostname] ip route add 
    default dev [corresponding port]'. The per destination route was set up with 'ip route add', with the
    syntax being '[router] ip route add [destination ip]/32 dev [corresponding port]'. The /32 was to
    ensure that only this exact ip address is specified as the destination without any subnet masks. 

2) Are there known issues or functions that aren't working currently in your attached code? If so, explain.

    There are no known issues or functions that are not working currently in the attached files.

3) What problems did you face developing code for this project?

    The only problem we encountered was that we did not have traceroute installed. This was fixed with two
    command line prompts. 

4) What did you learn by working on this project?

    By working on this project, we learned the general concepts behind setting up a network. We can specify
    what the routes between hosts and routers are on such networks. We can set up default routes for the
    network to function. We can then also set up the routing table on a per-destination basis and specify
    the route that we want packets to take. 
